#!/bin/bash
source /etc/profile
CONFIG_INI_FILE=$1
exportParams(){
  PARAM_FILE=$1
  ! [ -a ${PARAM_FILE} ] && return 1
  PARAMS=($(cat ${PARAM_FILE} | grep -v '#' | awk NF))
  for PARAM in "${PARAMS[@]}"
  do
    KEY=$(echo "${PARAM}" | awk -F '=' '{print $1}')
    VALUE=$(echo "${PARAM}" | awk -F '=' '{print $2}')
    eval "${KEY}=${VALUE}"
  done
}
exportParams ${CONFIG_INI_FILE}
DEPLOY_FILE_DIR=${DEPLOY_POSITION_DIR}/${DEPLOY_FILE_DIR_NAME}
rm -rf ${DEPLOY_FILE_DIR}
mv -f ~/${DEPLOY_FILE_DIR_NAME} ${DEPLOY_POSITION_DIR}
DEPLOY_ARCHIVE_FILE_DIR=${DEPLOY_FILE_DIR}/archived
CONFIG_INI_FILE=${DEPLOY_FILE_DIR}/config.ini
chmod 744 ${DEPLOY_FILE_DIR}/jenkins-deploy.sh
[ ${REMOTE_DEPLOY} == YES ] && ${DEPLOY_FILE_DIR}/jenkins-deploy.sh ${CONFIG_INI_FILE} > ${DEPLOY_FILE_DIR}/deploy.log
! [ ${REMOTE_SAVE_DEPLOY_FILE} == YES ] && rm -rf ${DEPLOY_FILE_DIR}
exit 0