#!/bin/bash

CONFIG_INI_FILE=$1

if ! [ -a "${CONFIG_INI_FILE}" ];
then
  echo "config not exist!"
  exit 1
fi
exportParams(){
  PARAM_FILE=$1
  ! [ -a ${PARAM_FILE} ] && return 1
  PARAMS=($(cat ${PARAM_FILE} | grep -v '#' | awk NF))
  for PARAM in "${PARAMS[@]}"
  do
    KEY=$(echo "${PARAM}" | awk -F '=' '{print $1}')
    VALUE=$(echo "${PARAM}" | awk -F '=' '{print $2}')
    eval "${KEY}=${VALUE}"
  done
}
exportParams ${CONFIG_INI_FILE}

DEPLOY_ARCHIVED_FILE_DIR=${PROJECT_DIR}/jenkins_deploy/${DEPLOY_FILE_DIR_NAME}/archived

#创建archived文件夹
mkdir -p ${DEPLOY_ARCHIVED_FILE_DIR}

#提取生成的 jar 与 配置文件
echo "start archive"
#压缩文件
DIST_DIR=${PROJECT_DIR}/dist
DIST_ZIP_FILE=${PROJECT_DIR}/dist.zip
if ! [ -d ${DIST_DIR} ];
then
  echo "dist dir not found！"
  exit 1
fi
echo "zip dist"
cd ${PROJECT_DIR} || exit 1
zip ${DIST_ZIP_FILE} -r dist/* > /dev/null

mkdir -p "${DEPLOY_ARCHIVED_FILE_DIR}/${module}"
rm -rf ${DEPLOY_ARCHIVED_FILE_DIR:?}/${module}/*
#copy dizt
mv ${DIST_ZIP_FILE} ${DEPLOY_ARCHIVED_FILE_DIR}/${module}/dist.zip


