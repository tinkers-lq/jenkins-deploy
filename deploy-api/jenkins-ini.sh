#!/bin/bash

#如果没有指定modules则收集所有modules
if ! [ -n "${modules}" ];
then
  if [ ${MICROSERVICE_ARC} == YES ];
  then
    for dir in $(ls ${PROJECT_DIR})
    do
      if [ -d ${PROJECT_DIR}/${dir} ] && [ -a ${PROJECT_DIR}/${dir}/pom.xml ] ;
      then
        modules="${modules}$([ -n "${modules}" ] && echo ',')${dir}"
      fi
    done
  else
    echo "非微服务架构需要指定modules"
    exit 1
  fi
fi

DEPLOY_FILE_DIR_NAME=${JOB_BASE_NAME}_deploy
DEPLOY_FILE_DIR=${PROJECT_DIR}/jenkins_deploy/${DEPLOY_FILE_DIR_NAME}
JENKINS_DIR=${PROJECT_DIR}/jenkins
#创建归档文件夹DEPLOY_FILE_DIR
[ -d ${DEPLOY_FILE_DIR} ] && rm -rf ${DEPLOY_FILE_DIR}
mkdir -p ${DEPLOY_FILE_DIR}

mv ${JENKINS_DIR}/jenkins-collect.sh ${DEPLOY_FILE_DIR}
mv ${JENKINS_DIR}/jenkins-deploy.sh ${DEPLOY_FILE_DIR}
[ ${REMOTE_DEPLOY} == YES ] && mv ${JENKINS_DIR}/jenkins-deploy-remote.sh ${DEPLOY_FILE_DIR}


#生成ini文件
CONFIG_INI_FILE=${DEPLOY_FILE_DIR}/config.ini
echo "BUILD_ID=${BUILD_ID}" >> ${CONFIG_INI_FILE}
echo "modules=${modules}" >> ${CONFIG_INI_FILE}
echo "PROJECT_DIR=${PROJECT_DIR}" >> ${CONFIG_INI_FILE}
echo "DEPLOY_POSITION_DIR=${DEPLOY_POSITION_DIR}" >>${CONFIG_INI_FILE}
echo "CONFIG_FILE_NAME=${CONFIG_FILE_NAME}" >> ${CONFIG_INI_FILE}
echo "MICROSERVICE_ARC=${MICROSERVICE_ARC}" >> ${CONFIG_INI_FILE}
echo "AUTO_BACKUP=${AUTO_BACKUP}" >> ${CONFIG_INI_FILE}
echo "COLLECT_APPLICATION_PROPS=${COLLECT_APPLICATION_PROPS}" >> ${CONFIG_INI_FILE}
echo "CHAIN_START=${CHAIN_START}" >> ${CONFIG_INI_FILE}
echo "CHAIN_START_ATOMICITY=${CHAIN_START_ATOMICITY}" >> ${CONFIG_INI_FILE}
echo "START_TIMEOUT=${START_TIMEOUT}" >> ${CONFIG_INI_FILE}
echo "REMOTE_DEPLOY=${REMOTE_DEPLOY}" >> ${CONFIG_INI_FILE}
echo "LOCAL_SAVE_DEPLOY_FILE=${LOCAL_SAVE_DEPLOY_FILE}" >> ${CONFIG_INI_FILE}
#远程部署相关配置
echo "REMOTE_SAVE_DEPLOY_FILE=${REMOTE_SAVE_DEPLOY_FILE}" >> ${CONFIG_INI_FILE}

#内部属性
echo "DEPLOY_FILE_DIR_NAME=${DEPLOY_FILE_DIR_NAME}" >> ${CONFIG_INI_FILE}