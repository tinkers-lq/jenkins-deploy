#  Jenkins 自动部署脚本使用手册

##  注意：编写时 默认已安装jenkins 以及能对jenkins进行基本操作

## 一、jenkins插件
Maven Integration plugin (创建maven结构的job)  
Publish Over SSH (ssh远程机器)
## 二、Gitee地址
https://gitee.com/tinkers-lq/jenkins-deploy.git
## 三、使用方法
### 1.拉取gitee上的代码  
### 2.在工程根目录下创建jenkins目录 web端同理
<font size=3>并根据需求将对应脚本copy到jenkins目录下(无需拷贝params.list) <br/> api部署 => deploy-api web部署 => deploy-front </font>  
![alt 属性文本](./image/jenkins目录位置.jpg)  
### 3.API端jenkins配置方法
> 1. 进入jenkins创建一个maven工程进入配置页面
> 
> ![alt 属性文本](./image/jenkins-maven-create.png)
> 2. 在源码管理处添加对应工程的git地址 并添加git账号 (过程略 百度)  
> 
> ![alt 属性文本](./image/jenkins-git.png)
> 3. 在 Pre Steps处 添加 执行shell 并添加配置参数 将gitee中deploy-api中的params.list粘贴在此  
> 4. 在 Pre Steps处 继续添加 Set the build result，Result选为成功  
> 5. 在 Build处 点击高级 填写相关信息如图所示  
> 
> ![alt 属性文本](./image/jenkins-maven.png)  
> 6.在 Post Steps处添加 执行shell 添加以下shell代码
> ```shell
> #!/bin/bash
> DEPLOY_FILE_DIR=${WORKSPACE}/jenkins_deploy/${JOB_BASE_NAME}_deploy
> CONFIG_INI_FILE=${DEPLOY_FILE_DIR}/config.ini
> REMOTE_DEPLOY=$(cat ${CONFIG_INI_FILE} | grep -E -w "REMOTE_DEPLOY" | awk -F "=" '{print $2}')
> ${DEPLOY_FILE_DIR}/jenkins-collect.sh ${CONFIG_INI_FILE}
> if ! [ ${REMOTE_DEPLOY} == YES ];
> then
> ${DEPLOY_FILE_DIR}/jenkins-deploy.sh ${CONFIG_INI_FILE}
> fi
> ```
> 7.在 Post Steps处继续添加 Send files or execute commands over SSH (远程部署需要)  
> &nbsp;&nbsp;&nbsp;SSH Server 需要现在系统配置中添加（操作步骤不做介绍，百度）  
> &nbsp;&nbsp;&nbsp;在 Source files 处填写 jenkins_deploy/**  
> &nbsp;&nbsp;&nbsp;在 Remove prefix 处填写 jenkins_deploy  
> &nbsp;&nbsp;&nbsp;在 Exec command 添加以下代码
> ```shell
> DEPLOY_FILE_DIR_NAME=${JOB_BASE_NAME}_deploy
> chmod 777 ~/${DEPLOY_FILE_DIR_NAME}/jenkins-deploy-remote.sh
> ~/${DEPLOY_FILE_DIR_NAME}/jenkins-deploy-remote.sh ~/${DEPLOY_FILE_DIR_NAME}/config.ini
> ```
> ![alt 属性文本](./image/jenkins-ssh.png)
> 8. 在 Post Steps处继续添加 执行shell 添加以下shell代码
> ```shell
> #!/bin/bash
> DEPLOY_FILE_DIR=${WORKSPACE}/jenkins_deploy/${JOB_BASE_NAME}_deploy
> CONFIG_INI_FILE=${DEPLOY_FILE_DIR}/config.ini
> LOCAL_SAVE_DEPLOY_FILE=$(cat ${CONFIG_INI_FILE} | grep -E -w "LOCAL_SAVE_DEPLOY_FILE" | awk -F "=" '{print $2}')
> if ! [ ${LOCAL_SAVE_DEPLOY_FILE} == YES ];
> then
> rm -rf ${WORKSPACE}/jenkins_deploy
> fi
> ```
### 4. front-web端jenkins配置方法
> 1. 进入jenkins创建一个自由风格工程并进入配置页面
> ![alt 属性文本](./image/jenkins-web-create.png)
> 2. 在源码管理处添加对应工程的git地址 并添加git账号 (过程略 百度)
> ![alt 属性文本](./image/jenkins-git.png)
> 3. 在 构建处 添加 执行shell 并添加配置参数 将gitee中deploy-front中的params.list粘贴在此
> 4. 在 构建处 继续添加 Set the build result，Result选为成功  
> 5. 在 构建处 继续添加 执行shell 添加以下代码  
> ```shell
> #如果不需要安装依赖 直接使用#号将 npm install 注释掉即可
> npm install
> rm -rf ${WORKSPACE:?}/dist
> npm run build
> ```
> 6.在 构建处 继续添加 执行shell 添加以下shell代码
> ```shell
> #!/bin/bash
> DEPLOY_FILE_DIR=${WORKSPACE}/jenkins_deploy/${JOB_BASE_NAME}_deploy
> CONFIG_INI_FILE=${DEPLOY_FILE_DIR}/config.ini
> REMOTE_DEPLOY=$(cat ${CONFIG_INI_FILE} | grep -E -w "REMOTE_DEPLOY" | awk -F "=" '{print $2}')
> ${DEPLOY_FILE_DIR}/jenkins-collect.sh ${CONFIG_INI_FILE}
> if ! [ ${REMOTE_DEPLOY} == YES ];
> then
> ${DEPLOY_FILE_DIR}/jenkins-deploy.sh ${CONFIG_INI_FILE}
> fi
> ```
> 7.在 构建处 继续添加 Send files or execute commands over SSH (远程部署需要)  
> &nbsp;&nbsp;&nbsp;SSH Server 需要现在系统配置中添加（操作步骤不做介绍，百度）  
> &nbsp;&nbsp;&nbsp;在 Source files 处填写 jenkins_deploy/**  
> &nbsp;&nbsp;&nbsp;在 Remove prefix 处填写 jenkins_deploy  
> &nbsp;&nbsp;&nbsp;在 Exec command 添加以下代码
> ```shell
> DEPLOY_FILE_DIR_NAME=${JOB_BASE_NAME}_deploy
> chmod 777 ~/${DEPLOY_FILE_DIR_NAME}/jenkins-deploy-remote.sh
> ~/${DEPLOY_FILE_DIR_NAME}/jenkins-deploy-remote.sh ~/${DEPLOY_FILE_DIR_NAME}/config.ini
> ```
> 
> ![alt 属性文本](./image/jenkins-ssh.png)
> 8. 在 构建处 继续添加 执行shell 添加以下shell代码
> ```shell
> #!/bin/bash
> DEPLOY_FILE_DIR=${WORKSPACE}/jenkins_deploy/${JOB_BASE_NAME}_deploy
> CONFIG_INI_FILE=${DEPLOY_FILE_DIR}/config.ini
> LOCAL_SAVE_DEPLOY_FILE=$(cat ${CONFIG_INI_FILE} | grep -E -w "LOCAL_SAVE_DEPLOY_FILE" | awk -F "=" '{print $2}')
> if ! [ ${LOCAL_SAVE_DEPLOY_FILE} == YES ];
> then
> rm -rf ${WORKSPACE}/jenkins_deploy
> fi
> ```
###参数说明

| 参数名 | 说明 | 取值 |
| :----: | :----- | :----: |
| modules | 微服务架构下所要部署的模块的名称使用逗号分割（支持单一或多模块部署），非微服务架构时为项目名称| string |
| DEPLOY_POSITION_DIR | 项目所要发布的位置 例如：/opt/demo | string |
| COLLECT_APPLICATION_PROPS | 是否收集应用配置文件 如为NO则发布时不会对配置文件进行替换（发布时若无配置文件模块不会进行启动但可以将文件发布到相应位置） | YES/NO |
| CONFIG_FILE_NAME | 需要提取的配置文件的名称 例如：application-dev.properties| string |
| AUTO_BACKUP | 是否自动备份 备份位置在${DEPLOY_POSITION_DIR}对应模块下，名称格式为yyyy.mm.dd | YES/NO |
| CHAIN_START | 是否为链式启动，微服务架构时多模块部署可按modules指定顺序启动 | YES/NO |
| CHAIN_START_ATOMICITY | 链式原子启动，当为链式启动时只要有一个服务启动失败则会kill已经启动的服务，除非全部启动成功 | YES/NO |
| START_TIMEOUT | 链式启动下，每个服务的启动超时时间（单位 s） | 数字 |
| REMOTE_DEPLOY | 是否为远程部署（本地指jenkins所在的机器除此只外都为远程） | YES/NO |
| REMOTE_SAVE_DEPLOY_FILE | 远程部署时在远端是否留存部署文件(包含部署日志+归档文件+执行脚本) | YES/NO |
| LOCAL_SAVE_DEPLOY_FILE | 部署时本地是否留存部署文件(归档文件+执行脚本) | YES/NO |
| MICROSERVICE_ARC | 标志是否为微服务架构 | YES/NO |
| PROJECT_DIR | 项目路径 （使用jenkins部署时一般为${WORKSPACE}） | string |